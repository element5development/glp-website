<?php 
/*-------------------------------------------------------------------
    Template Name: Styleguide
-------------------------------------------------------------------*/
?>

<?php get_header(); ?>
<div class="main-container">
  <section class="styleguide-section styleguide-section--title">
    <h1><?php echo get_bloginfo( 'name' ); ?></h1>
    <?php if ( get_option('blogdescription') ) { ?>
      <h2><?php echo get_option('blogdescription'); ?></h2>
    <?php } ?>
    <hr>
    <p>Below you will find <?php echo get_bloginfo( 'name' ); ?>'s styleguide. This style guide provides all the branding resources you will need when writing and/or designing documents on or for <?php echo get_bloginfo( 'name' ); ?>.</p>
  </section>
  <section class="styleguide-section">
    <h2>Logo</h2>
    <div class="styleguide-section__sub"> 
      <h3>Primary Logo</h3>
      <div class="primary-logo">
        <?php $logo = get_field('primary_logo'); ?> 
        <a target="_blank" href="<?php echo $logo['url']; ?>">
          <img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" title="<?php echo $logo['alt']; ?>" />
        </a>
      </div>
    </div>
    <?php if( have_rows('logo_variant') ) { ?>
      <div class="styleguide-section__sub"> 
        <h3>Variants</h3>
        <div class="block">
          <?php while ( have_rows('logo_variant') ) : the_row(); $i++; ?>
            <div class="logo-varient">
              <div class="varient-image" style="background-color: <?php the_sub_field('background_color'); ?>">
                <?php $logo = get_sub_field('logo'); ?> 
                <a target="_blank" href="<?php echo $logo['url']; ?>">
                  <img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" title="<?php echo $logo['alt']; ?>" />
                </a>
              </div>
              <div class="logo-varient__description">
                <p><?php the_sub_field('description'); ?></p>
              </div>
            </div>
          <?php endwhile; ?>
        </div>
      </div>
    <?php } else {
      //NOTHING
    } ?>
  </section>
  <section class="styleguide-section">
    <h2>Colors</h2>
    <div class="styleguide-section__sub"> 
      <h3>Primary Colors</h3>
      <div class="block">
        <?php while ( have_rows('primary_colors') ) : the_row(); ?>
          <div class="color">
            <div class="color__swatch" style="background-color: <?php the_sub_field('color'); ?>;"></div>
            <div class="color__details">
              <?php
                $rgb = hex2rgb( get_sub_field('color') );
                $cmyk = rgb2cmyk(hex2rgb2( get_sub_field('color') ));
              ?>
              <h4><?php the_sub_field('color_name'); ?></h4>
              <p><?php the_sub_field('color'); ?> | <?php the_sub_field('color_pantone'); ?></p>
              <p><?php echo $rgb; ?></p>
              <p><?php echo $cmyk; ?></p>
            </div>
          </div>
        <?php endwhile; ?>
      </div>
    </div>
    <?php if( have_rows('secondary_colors') ) { ?>
      <div class="styleguide-section__sub"> 
        <a id="secondary-colors" class="anchor"></a> 
        <h3>Secondary Colors</h3>
        <div class="block">
          <?php while ( have_rows('secondary_colors') ) : the_row(); ?>
            <div class="color">
              <div class="color__swatch" style="background-color: <?php the_sub_field('color'); ?>;"></div>
              <div class="color__details">
                <?php
                  $rgb = hex2rgb( get_sub_field('color') );
                  $cmyk = rgb2cmyk(hex2rgb2( get_sub_field('color') ));
                ?>
                <h4><?php the_sub_field('color_name'); ?></h4>
                <p><?php the_sub_field('color'); ?> | <?php the_sub_field('color_pantone'); ?></p>
                <p><?php echo $rgb; ?></p>
                <p><?php echo $cmyk; ?></p>
              </div>
            </div>
          <?php endwhile; ?>
        </div>
      </div>
    <?php } ?>
  </section>
  <?php if( have_rows('icons') ) { ?>
    <section class="styleguide-section">
      <h2>Icons</h2>
      <div class="styleguide-section__sub">
        <div class="block">
          <?php while ( have_rows('icons') ) : the_row(); ?>
            <div class="brand-icon">
              <?php $icon = get_sub_field('icon'); ?>
              <a target="_blank" href="<?php echo $icon['url']; ?>">
                <img src="<?php echo $icon['url']; ?>" alt="<?php echo $logo['alt']; ?>" title="<?php echo $logo['alt']; ?>" />
              </a>
            </div>
          <?php endwhile; ?>
        </div>
        <?php if( get_field('icon_zip_download') ) { ?>
          <a href="<?php the_field('icon_zip_download'); ?>" class="btn">Download All</a>
        <?php } ?>
      </div>
    </section>
  <?php } ?>
  <?php if( have_rows('imagery') ) { ?>
    <section class="styleguide-section">
      <h2>Imagery</h2>
      <div class="styleguide-section__sub">
        <div class="block">
          <?php while ( have_rows('imagery') ) : the_row(); ?>
            <div class="brand-imagery">
              <?php $image = get_sub_field('image'); $size = 'large'; $thumb = $image['sizes'][ $size ]; ?>
              <a target="_blank" href="<?php echo $image['url']; ?>">
                <img src="<?php echo $thumb; ?>" alt="<?php echo $image['alt']; ?>" />
              </a>
            </div>
          <?php endwhile; ?>
        </div>
        <?php if( get_field('imagery_zip_download') ) { ?>
          <a href="<?php the_field('pattern_zip_download'); ?>" class="primary-button">Download All</a>
        <?php } ?>
      </div>
    </section>
  <?php } ?>
  <section class="styleguide-section">
    <h2>Typography</h2>
    <div class="styleguide-section__sub"> 
      <a id="typeface" class="anchor"></a> 
      <h3>Typeface</h3>
      <div class="block">
        <?php while ( have_rows('typeface') ) : the_row(); ?>
          <div class="font-style">
              <p style="<?php echo "font-weight: ".get_sub_field('font_weight').";";?>">Aa Zz</p>
              <p><?php the_sub_field('font_name'); ?></p>
          </div>
        <?php endwhile; ?>
      </div>
    </div>
    <div class="styleguide-section__sub"> 
      <h3>Headlines</h3>
      <div class="headline-option">
        <h1>Heading One</h1>
        <p>
          <?php 
            $base = 16;
            $font = $base * 3.5;
            $rgb = hex2rgb( '#000000' );
            $cmyk = rgb2cmyk(hex2rgb2( '#000000' ));
          ?>
          3.5em/107% or <?php echo $font; ?>px/60px
          | Font Weight: 800
          | Char; Spacing: 0px 
          | #000000 or <?php echo $rgb; ?> or <?php echo $cmyk; ?>
        </p>
      </div>
      <div class="headline-option">
        <h1></h1><h2>Subheading for Heading One</h2>
        <p>
          <?php 
            $font = $base * 2.5;
            $rgb = hex2rgb( '#000000' );
            $cmyk = rgb2cmyk(hex2rgb2( '#000000' ));
          ?>
          2.5em/107% or <?php echo $font; ?>px/42.8px
          | Font Weight: 800
          | Char; Spacing: 0px 
          | #000000 or <?php echo $rgb; ?> or <?php echo $cmyk; ?>
        </p>
      </div>
      <div class="headline-option">
        <h2>Heading Two</h2>
        <p>
          <?php 
            $font = $base * 3;
            $rgb = hex2rgb( '#000000' );
            $cmyk = rgb2cmyk(hex2rgb2( '#000000' ));
          ?>
          3em/107% or <?php echo $font; ?>px/56px
          | Font Weight: 800
          | Char; Spacing: 0px 
          | #000000 or <?php echo $rgb; ?> or <?php echo $cmyk; ?>
        </p>
      </div>
      <div class="headline-option">
        <h2></h2><h3>Subheading for Heading Two</h3>
        <p>
          <?php 
            $font = $base * 3;
            $rgb = hex2rgb( '#000000' );
            $cmyk = rgb2cmyk(hex2rgb2( '#000000' ));
          ?>
          1.65em/107% or <?php echo $font; ?>px/56px
          | Font Weight: 800
          | Char; Spacing: 0px 
          | #000000 or <?php echo $rgb; ?> or <?php echo $cmyk; ?>
        </p>
      </div>
      <div class="headline-option">
        <h3>Heading Three</h3>
        <p>
          <?php 
            $font = $base * 1.5;
            $rgb = hex2rgb( '#000000' );
            $cmyk = rgb2cmyk(hex2rgb2( '#000000' ));
          ?>
          1.5em/107% or <?php echo $font; ?>px/28px
          | Font Weight: 800
          | Char; Spacing: 0px 
          | #000000 or <?php echo $rgb; ?> or <?php echo $cmyk; ?>
        </p>
      </div>
      <div class="headline-option">
        <h4>Heading Four</h4>
        <p>
          <?php 
            $font = $base * 1.25;
            $rgb = hex2rgb( '#000000' );
            $cmyk = rgb2cmyk(hex2rgb2( '#000000' ));
          ?>
          1.25em/107% or <?php echo $font; ?>px/22px
          | Font Weight: 800
          | Char; Spacing: 0px 
          | #000000 or <?php echo $rgb; ?> or <?php echo $cmyk; ?>
        </p>
      </div>
      <div class="headline-option">
        <h5>Heading Five</h5>
        <p>
          <?php  
            $font = $base * 1.25;
            $rgb = hex2rgb( '#000000' );
            $cmyk = rgb2cmyk(hex2rgb2( '#000000' ));
          ?>
          1.25em/107% or <?php echo $font; ?>px/22px
          | Font Weight: 800
          | Char; Spacing: 0px 
          | #000000 or <?php echo $rgb; ?> or <?php echo $cmyk; ?>
        </p>
      </div>
      <div class="headline-option">
        <h6>Heading Six</h6>
        <p>
          <?php  
            $font = $base * 1.125;
            $rgb = hex2rgb( '#000000' );
            $cmyk = rgb2cmyk(hex2rgb2( '#000000' ));
          ?>
          1.125em/107% or <?php echo $font; ?>px/20px
          | Font Weight: 800
          | Char; Spacing: 0px 
          | #000000 or <?php echo $rgb; ?> or <?php echo $cmyk; ?>
        </p>
      </div>
      <div class="headline-option">
        <p>Body Content</p>
        <p>
          <?php 
            $font = $base * 1;
            $rgb = hex2rgb( '#000000' );
            $cmyk = rgb2cmyk(hex2rgb2( '#000000' ));
          ?>
          1em/137.5% or <?php echo $font; ?>px/22px
          | Font Weight: 400 
          | Char; Spacing: 0px 
          | #000000 or <?php echo $rgb; ?> or <?php echo $cmyk; ?>
        </p>
      </div>
      <div class="headline-option">
        <blockquote>Block Quote</blockquote>
        <p>
          <?php 
            $font = $base * 1.625;
            $rgb = hex2rgb( '#000000' );
            $cmyk = rgb2cmyk(hex2rgb2( '#000000' ));
          ?>
          1.625em/138% or <?php echo $font; ?>px/35px
          | Font Weight: 400 
          | Char; Spacing: 0px 
          | #000000 or <?php echo $rgb; ?> or <?php echo $cmyk; ?>
        </p>
      </div>
    </div>
    <div class="styleguide-section__sub"> 
      <h3>Text Formatting</h3>
      <div class="editor-contents">
        <?php the_field('text_formatting'); ?>
      </div>
    </div>
  </section>
  <section class="styleguide-section">
    <a id="styleguide-buttons" class="anchor"></a>
    <h2>Buttons</h2>
    <div class="styleguide-section__sub"> 
      <a id="p-buttons" class="anchor"></a> 
      <h3>Shortcodes</h3>
      <div class="block">
        <?php the_field('btn_shortcodes') ?>
      </div>
    </div>
    <div class="styleguide-section__sub"> 
      <a id="social-icons" class="anchor"></a> 
      <h3>Social Icons</h3>
    </div>
  </section>
  <section class="styleguide-section">
    <a id="styleguide-forms" class="anchor"></a>
    <h2>Forms</h2>
    <?php echo do_shortcode('[gravityform id="1" title="false" description="false"]'); ?>
  </section>
</div>

<?php get_footer(); ?>