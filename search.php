<?php get_header(); ?>
<section class="search-banner-section">
  <div class="content">
    <h1>Search Results for <?php the_search_query(); ?></h1>
  </div>
</section>
<section class="search-wrap">
	<div class="content">
		<?php if (!have_posts()) : ?>
			<p>Sorry, no results were found. Try searching again by using the magnifying glass at the top of this page.</p>
		<?php endif; ?>

		<?php while (have_posts()) : the_post(); ?>
			<?php get_template_part('template-parts/pages/content', 'search'); ?>
		<?php endwhile; ?>

		<?php the_posts_navigation(); ?>
	</div>
</section>
<?php get_footer(); ?>