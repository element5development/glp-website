<?php 
/*-------------------------------------------------------------------
    Template Name: Homepage
-------------------------------------------------------------------*/
?>
<?php get_header(); ?>

<?php get_template_part('template-parts/elements/home-banner'); ?>
<?php get_template_part('template-parts/elements/home-content'); ?>

<?php get_footer(); ?>