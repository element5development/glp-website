var $ = jQuery;

$(document).ready(function(){
  $('#edu-yes').on('click', function(){
    if($('.form-wrapper .no-edu-form').css('display') === 'block'){
      $('.form-wrapper .no-edu-form').slideToggle(200);
      $('.form-wrapper .edu-form').delay(200).slideToggle(250);
    } else{
      $('.form-wrapper .edu-form').slideToggle(250);
    }
  });

  $('#edu-no').on('click', function(){
    if($('.form-wrapper .edu-form').css('display') === 'block'){
      $('.form-wrapper .edu-form').slideToggle(200);
      $('.form-wrapper .no-edu-form').delay(200).slideToggle(250);
    } else{
      $('.form-wrapper .no-edu-form').slideToggle(250);
    }
  });

  if(window.location.href.indexOf("#gf_3") > -1) {
    $('.form-wrapper .no-edu-form').show();
  } else if(window.location.href.indexOf("#gf_2") > -1){
    $('.form-wrapper .edu-form').show();
  }

  $('.gf_step_number').prepend('Step ');
});