var $ = jQuery;

$(document).ready(function(){
  var window_width = $(window).width();

  $('.search-icon').on('click', function(){
    $('.search-dropdown').slideToggle('250ms');
  });

  $('.search-icon-mobile').on('click', function(){
    $('.search-dropdown').slideToggle('250ms');
  });

  $('#dir-btn').on('click', function(){
    $('.directions-modal-contain').show().removeClass('zoomOut').addClass('animated zoomIn');
  });

  $('#modal-close').on('click', function(){
    $('.directions-modal-contain').removeClass('zoomIn').addClass('zoomOut').delay(200).hide(0);
  });

  ///screen specific functions
  if (window_width >= 980){
    $('.nav-primary').removeAttr('style');
    $('.nav-primary').removeClass('animated zoomOut zoomIn');
  }

  if (window_width <= 979){
    $('.mobile-menu-icon').on('click', function(){   
      $('.nav-primary').show().removeClass('zoomOut').addClass('animated zoomIn');
    });
    
    $('.mobile-close-button').on('click', function(){
      $('.nav-primary').removeClass('zoomIn').addClass('zoomOut').delay(200).hide(0);
    });
  }

  $(window).resize(function() {
    var window_width = $(window).width();
    if (window_width >= 980){
      $('.nav-primary').removeAttr('style');
      $('.nav-primary').removeClass('animated zoomOut zoomIn');
    }

    if (window_width <= 979) {
      $('.mobile-menu-icon').on('click', function(){   
        $('.nav-primary').show().removeClass('zoomOut').addClass('animated zoomIn');
      });
      
      $('.mobile-close-button').on('click', function(){
        $('.nav-primary').removeClass('zoomIn').addClass('zoomOut').delay(200).hide(0);
      });
    }
  });
});