var $ = jQuery;

$(document).ready(function() {
  $('.slider-contain').slick({
    mobileFirst: true,
    prevArrow: '<a class="prev-slide-contain"></a>',
    nextArrow: '<a class="next-slide-contain"></a>',
    fade: true,
    cssEase: 'linear',
    autoplay: true,
    autoplaySpeed: 3000
  });
});