<?php 
/*-------------------------------------------------------------------
    Template Name: Book Appointment Page Layout
-------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/default-banner'); ?>
<?php get_template_part('template-parts/navigation/default-breadcrumbs'); ?>
<?php get_template_part('template-parts/elements/book-content'); ?>
<?php get_template_part('template-parts/elements/book-form'); ?>


<?php get_footer(); ?>