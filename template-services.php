<?php 
/*-------------------------------------------------------------------
    Template Name: Services Page Layout
-------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/default-banner'); ?>
<?php get_template_part('template-parts/navigation/default-breadcrumbs'); ?>
<?php get_template_part('template-parts/elements/services-content'); ?>


<?php get_footer(); ?>