<?php 
  add_filter('relevanssi_excerpt_content', 'custom_fields_to_excerpts', 10, 3);
  function custom_fields_to_excerpts($content, $post, $query) {

    $custom_field = get_post_meta($post->ID, 'about_section_one_content', true);
    $content .= " " . $custom_field;

    $custom_field = get_post_meta($post->ID, 'about_section_two_cards', true);
    $content .= " " . $custom_field;

    $custom_field = get_post_meta($post->ID, 'banner_headline', true);
    $content .= " " . $custom_field;

    $custom_field = get_post_meta($post->ID, 'book_content', true);
    $content .= " " . $custom_field;

    $custom_field = get_post_meta($post->ID, 'careers_section_one_content', true);
    $content .= " " . $custom_field;

    $custom_field = get_post_meta($post->ID, 'careers_section_three_heading', true);
    $content .= " " . $custom_field;

    $custom_field = get_post_meta($post->ID, 'careers_section_three_content', true);
    $content .= " " . $custom_field;

    $custom_field = get_post_meta($post->ID, 'careers_section_three_looking_for_tiles', true);
    $content .= " " . $custom_field;

    $custom_field = get_post_meta($post->ID, 'careers_section_four_content', true);
    $content .= " " . $custom_field;

    $custom_field = get_post_meta($post->ID, 'careers_section_six_content', true);
    $content .= " " . $custom_field;

    $custom_field = get_post_meta($post->ID, 'careers_section_seven_content', true);
    $content .= " " . $custom_field;

    $custom_field = get_post_meta($post->ID, 'slider_images', true);
    $content .= " " . $custom_field;

    $custom_field = get_post_meta($post->ID, 'homepage_section_1', true);
    $content .= " " . $custom_field;

    $custom_field = get_post_meta($post->ID, 'homepage_section_2_headline', true);
    $content .= " " . $custom_field;

    $custom_field = get_post_meta($post->ID, 'homepage_section_2_text', true);
    $content .= " " . $custom_field;

    $custom_field = get_post_meta($post->ID, 'service_content_one', true);
    $content .= " " . $custom_field;

    $custom_field = get_post_meta($post->ID, 'section_two_service_items', true);
    $content .= " " . $custom_field;

    $custom_field = get_post_meta($post->ID, 'service_content_section_three_headline', true);
    $content .= " " . $custom_field;

    $custom_field = get_post_meta($post->ID, 'service_section_three_bullet_points', true);
    $content .= " " . $custom_field;

    return $content;
  } 
?>