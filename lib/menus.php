<?php

/*-----------------------------------------
		MENUS - www.wp-hasty.com
-----------------------------------------*/
function nav_creation() {
	$locations = array(
		'primary_nav' => __( 'Primary Navigation'),
	);
	register_nav_menus( $locations );

}
add_action( 'init', 'nav_creation' );