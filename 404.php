<?php get_header(); ?>

<section class="search-banner-section">
  <div class="content">
    <h1>Page Not Found</h1>
  </div>
</section>

<section class="search-wrap">
	<div class="content">
		<p><strong>404 - Article Not Found</strong></p>
		<p>The article you were looking for was not found, but maybe try looking again!</p>
	</div>
</section>

<?php get_footer(); ?>
