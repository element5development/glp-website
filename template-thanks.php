<?php 
/*-------------------------------------------------------------------
    Template Name: Thank You Page Layout
-------------------------------------------------------------------*/
?>

<?php get_header('thanks'); ?>

<section class="thanks-wrap">
  <div class="content">
    <div class="close-contain">
     <a href="<?= esc_url(home_url('/')); ?>"><img src="<?= get_template_directory_uri();?>/dist/images/thanks-close.svg" alt="Close Thanks Icon"></a>
    </div>
    <h1>
      Thanks
      <?php 
        if($_GET[fullname]); 
          echo '<br/>'.$_GET[fullname].'!';
      ?>
    </h1>
    <?php the_content(); ?>
    <div class="social-contain">
      <ul>
        <?php if( have_rows('social_media', 'option') ): ?>
          <?php while ( have_rows('social_media', 'option') ) : the_row(); ?>
            <li><a href="<?php the_sub_field('social_media_url'); ?>" target="_blank"><img src="<?php the_sub_field('social_media_icon'); ?>" alt="<?php the_sub_field('social_media_name'); ?>"></a></li>
          <?php endwhile; ?>
        <?php endif; ?>
      </ul>
    </div>
  </div>
</section>

<?php get_footer('thanks'); ?>