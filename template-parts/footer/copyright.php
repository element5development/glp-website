<div class="copyright-contain">
  <p>©Copyright <?php echo date("Y"); ?> GLP & Associates. All rights reserved. Crafted by <a href="https://element5digital.com/" target="_blank">Element5</a></p>
  <p class="desk-privacy"><a href="<?php get_site_url(); ?>/privacy-policy">Privacy Policy</a></p>
</div>