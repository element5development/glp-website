<?php 
$messyphone = get_field('primary_phone', 'option');
$cleanphone = preg_replace('/[^0-9]/','', $messyphone);
?>

<div class="footer-directions-wrap">
  <div class="wrap-address">
    <h2>GLP & Associates</h2>
		<?php 
			// displaying the values
			$address = get_field('address_select');
			if (!empty($address)) {
				if( have_rows('locations', 'option') ):
					while ( have_rows('locations', 'option') ) : the_row();
						if (have_rows('single_location', 'option')) {
							while (have_rows('single_location', 'option')) {
								the_row();
								$city = get_sub_field('city');
								if (in_array($city, $address)) {
									// get the other sub fields after checking 
									// instead of getting values that may not be needed
									$paddress = get_sub_field('street_address');
									$pcity = get_sub_field('city');
									$pstate = get_sub_field('state_abbreviation');
									$pzip = get_sub_field('zipcode');
									?>
										<p>
											<?php echo $paddress; ?><br/> 
											<?php echo $pcity; ?>, <?php echo $pstate; ?> <?php echo $pzip; ?>
										</p>
									<?php 
								} // end if selection is selected
							} // end while have rows
						}  // end if get field
					endwhile;
				endif;
			} // end if !empty
		?>
    <div class="mobile-privacy">
      <p><a href="<?php get_site_url(); ?>/privacy-policy">Privacy Policy</a></p>
    </div>
  </div>
  <div class="call-btn-wrap">
    <div class="direction-call-btn-contain">
      <a class="square-btn" id="dir-btn" href="javascript:void(0)">Directions</a>
      <a class="square-btn" href="tel:+1<?php echo $cleanphone; ?>">
        <?php the_field('primary_phone', 'option') ?>
      </a>
    </div>
  </div>
</div>