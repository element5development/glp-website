<div class="footer-social-contain">
  <ul>
    <?php if( have_rows('social_media', 'option') ): ?>
      <?php while ( have_rows('social_media', 'option') ) : the_row(); ?>
        <li><a href="<?php the_sub_field('social_media_url'); ?>" target="_blank"><img src="<?php the_sub_field('social_media_icon'); ?>" alt="<?php the_sub_field('social_media_name'); ?>"></a></li>
      <?php endwhile; ?>
    <?php endif; ?>
  </ul>
</div>