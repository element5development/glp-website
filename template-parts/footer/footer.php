<footer>
  <?php get_template_part('template-parts/footer/footer-directions'); ?>
  <?php get_template_part('template-parts/footer/social-media'); ?>
  <?php get_template_part('template-parts/footer/copyright'); ?>
  <?php get_template_part('template-parts/navigation/mobile-bottom-menu'); ?>
  <?php get_template_part('template-parts/footer/directions-modal'); ?>
</footer>