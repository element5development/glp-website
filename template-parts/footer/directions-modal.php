<?php 
$messyphone = get_field('primary_phone', 'option');
$cleanphone = preg_replace('/[^0-9]/','', $messyphone);
?>

<div class="directions-modal-contain">
  <div class="content-contain">
    <div class="close-contain">
      <img id="modal-close" src="<?= get_template_directory_uri();?>/dist/images/mobile-close-button.svg" alt="Modal Close Button">
    </div>
    <div class="choose-wrap">
      <h2>Choose your location</h2>
      <p>Or call us toll free:</p>
      <a href="tel:+1<?php echo $cleanphone; ?>"><?php the_field('primary_phone', 'option') ?></a>
    </div>
    <div class="locations-wrap">
      <?php

        // check if the flexible content field has rows of data
        if( have_rows('locations', 'option') ):

          // loop through the rows of data
            while ( have_rows('locations', 'option') ) : the_row();

            // check current row layout
                if( get_row_layout() == 'location_info' ):
                  echo '<div class="state-wrap">';
                  echo '<div class="state">';
                  echo '<h2>'. get_sub_field('state_full_name') .'</h2>';
                  // check if the nested repeater field has rows of data
                  if( have_rows('single_location') ):
                  
                    echo '<ul>';

                    // loop through the rows of data
                      while ( have_rows('single_location') ) : the_row();

                      echo '<li><a class="square-btn" href="'. get_sub_field('google_maps_url') .'" target="_blank">'. get_sub_field('city') .'</a></li>';

                    endwhile;

                    echo '</ul>';

              endif;
                echo '</div>';
                echo '</div>';
                endif;

            endwhile;

        else :

            // no layouts found

        endif;

      ?>
    </div>
  </div>
</div>