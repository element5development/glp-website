<article>
  <a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
  <?php the_excerpt(); ?>
</article>