<section class="content-section-one" style="background-image: url(<?php the_field('homepage_section_1_background_image'); ?>">
  <div class="content-contain">
    <?php the_field('homepage_section_1'); ?>
    <?php 

      $link = get_field('homepage_section_1_button');

      if( $link ): ?>
        
        <a class="button" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>

    <?php endif; ?>
  </div>
  <div class="content-overlay"></div>
</section>

<section class="content-partners-section">
	<div class="title-contain">
		<h3><?php the_field('homepage_partners_section_headline'); ?></h3>
	</div>
  <div class="card-contain">
		<?php if( have_rows('homepage_partners_section_repeater') ): ?>

      <?php while ( have_rows('homepage_partners_section_repeater') ) : the_row(); ?>
        <div class="card">

					<?php
						$partnerlogo = get_sub_field('partner_logo');
					?>

					<img src="<?php echo $partnerlogo['url']; ?>" alt="<?php echo $partnerlogo['alt']; ?>">
					<p><?php the_sub_field('partner_name'); ?></p>

					<?php
						$partnerlink = get_sub_field('partner_link');
					?>

					<a class="button" href="<?php echo $partnerlink['url']; ?>" target="<?php echo $partnerlink['target']; ?>"><?php echo $partnerlink['title']; ?></a>

        </div>
      <?php endwhile; ?>

    <?php endif; ?>
  </div>
</section>

<section class="content-section-two">
  <div class="content-contain">
    <div class="wrap-content">
      <h2><?php the_field('homepage_section_2_headline'); ?></h2>
      <?php the_field('homepage_section_2_text'); ?>
    </div>
  </div>
</section>