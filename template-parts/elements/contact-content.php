<section class="form-contain">
  <div class="form-wrap">
    <h2>Contact Us</h2>
    <?php gravity_form( 4, false, false, false, '', false ); ?>
  </div>
</section>

<section class="main-content">
  <div class="content">
    <?php if( have_rows('contact_locations') ): ?>

      <?php while ( have_rows('contact_locations') ) : the_row(); ?>

        <?php if( get_row_layout() == 'state' ): ?>
          <div class="state-wrap">
            <div class="title-wrap">
              <h2><?php the_sub_field('contact_state_name'); ?></h2>
            </div>
            <?php if( have_rows('state_locations') ): ?>

              <?php while ( have_rows('state_locations') ) : the_row(); ?>
                <div class="location-wrap">
                  <h3><?php the_sub_field('location_title'); ?></h3>
                  <?php the_sub_field('location_info'); ?>
                </div>
              <?php endwhile; ?>

            <?php endif; ?>

          </div>
        <?php endif; ?>

      <?php endwhile; ?>

    <?php endif; ?>
  </div>
</section>