<section class="service-content-one">
  <div class="content">
    <?php the_field('service_content_one'); ?>
  </div>
</section>

<section class="service-content-two">
  <div class="content">
    <ul>
      <?php if( have_rows('section_two_service_items') ): ?>

            <?php while ( have_rows('section_two_service_items') ) : the_row(); 
              $image = get_sub_field('service_icon');
            ?>

              <li>
                <div class="img-wrap">
                  <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
                </div>
                <div class="headline-contain">
                  <h3><?php the_sub_field('service_title'); ?></h3>
                  <p><?php the_sub_field('service_sub_headline'); ?></p>
                </div>
              </li>

           <?php endwhile; ?>

        <?php endif; ?>
    </ul>
  </div>
</section>

<section class="service-content-three" style="background-image: url('<?php the_field('service_section_three_background_image'); ?>')">
  <div class="content">
    <h2><?php the_field('service_content_section_three_headline'); ?></h2>
    <ul>
      <?php if( have_rows('service_section_three_bullet_points') ): ?>
        <?php while ( have_rows('service_section_three_bullet_points') ) : the_row(); ?>
          <li><?php the_sub_field('bullet'); ?></li>
        <?php endwhile; ?>
      <?php endif; ?>
    </ul>
    <div class="button-container">
    <?php 

      $link = get_field('service_section_three_button');

      if( $link ): ?>
        
        <a class="button-green" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>

    <?php endif; ?>
    </div>
  </div>
</section>