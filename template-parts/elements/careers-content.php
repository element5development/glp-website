<section class="careers-one">
  <div class="content-wrap">
    <div class="image-wrap" style="background-image: url('<?php the_field('careers_section_one_background_image'); ?>')"></div>
    <div class="content-contain">
      <div class="content">
        <?php the_field('careers_section_one_content'); ?>
        <div class="button-wrap">
          <?php 

            $link = get_field('careers_section_one_button');

            if( $link ): ?>
              
              <a class="button" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>

          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="careers-three">
  <div class="content-wrapper">
    <div class="content-contain">
      <div class="content">
        <h2><?php the_field('careers_section_three_heading'); ?></h2>
        <p><?php the_field('careers_section_three_content'); ?></p>
        <h3>What we are looking for</h3>
      </div>
      <div class="tile-wrap">
        <div class="content-tiles">
          <?php if( have_rows('careers_section_three_looking_for_tiles') ): ?>

            <?php while ( have_rows('careers_section_three_looking_for_tiles') ) : the_row(); 
              $image = get_sub_field('icon');
            ?>
              <div class="career-path">
                <div class="tile-content-wrapper">
                  <div class="icon-contain">
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
                  </div>
                  <div class="path-title">
                    <h3><?php the_sub_field('title'); ?></h3>
                  </div>
                </div>
              </div>

            <?php endwhile; ?>

          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
  <div class="image-section" style="background-image: url('<?php the_field('careers_section_two_background_image'); ?>')"></div>
</section>

<section class="careers-four">
  <div class="content">
    <?php the_field('careers_section_four_content'); ?>
  </div>
  <div class="img-wrapper" style="background-image: url('<?php the_field('careers_section_four_image'); ?>')"></div>
</section>

<section class="careers-six">
  <div class="content-contain">
    <div class="content">
      <?php the_field('careers_section_six_content'); ?>
      <div class="button-wrap">
        <?php 

          $link = get_field('careers_section_six_button');

          if( $link ): ?>
            
            <a class="button-green" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>

        <?php endif; ?>
      </div>
    </div>
  </div>
  <div class="image-wrapper" style="background-image: url('<?php the_field('careers_section_five_background_image'); ?>')"></div>
</section>

<section class="careers-seven" style="background-image: url('<?php the_field('careers_section_seven_background_image'); ?>')">
  <div class="content">
    <?php 

      $image = get_field('careers_section_seven_icon');

      if( !empty($image) ): ?>

        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

    <?php endif; ?>
    <?php the_field('careers_section_seven_content'); ?>
    <div class="button-wrap">
    <?php 

        $link = get_field('careers_section_seven_button');

        if( $link ): ?>
          
          <a class="button" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>

    <?php endif; ?>
    </div>
  </div>
</section>