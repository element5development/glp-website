<section id="book-form-link" class="book-content-section">
  <div class="content">
    <?php the_field('book_content'); ?>
  </div>
</section>