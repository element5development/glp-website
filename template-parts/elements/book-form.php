<section class="book-form-section">
  <div class="content">
    <div class="educator-question">
      <h2>Are you an educator?</h2>
      <div class="boolean-buttons">
        <a href="javascript:void(0)" class="button" id="edu-yes">Yes</a>
        <a href="javascript:void(0)" class="button" id="edu-no">No</a>
      </div>
    </div>
  </div>
</section>

<section class="form-wrapper">
  <div class="content">
    <div class="edu-form">
      <?php gravity_form( 2, false, false, false, '', false ); ?>
    </div>
    <div class="no-edu-form">
      <?php gravity_form( 3, false, false, false, '', false ); ?>
    </div>
  </div>
</section>