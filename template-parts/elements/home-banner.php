<section class="home-banner-section">
  <div class="slider-contain">
  <?php

    // check if the repeater field has rows of data
    if( have_rows('slider_images') ):

      // loop through the rows of data
        while ( have_rows('slider_images') ) : the_row();

          echo '<div class="slide" style="background-image: url(' . get_sub_field('slide_image') . ')"><div class="content">' . get_sub_field('slide_caption') . '</div><div class="slide-overlay"></div></div>';

        endwhile;

    endif;

  ?>
  </div>
</section>