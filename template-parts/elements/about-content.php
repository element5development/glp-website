<section class="about-content-one">
  <div class="content">
    <?php the_field('about_section_one_content'); ?>
  </div>
</section>

<section class="about-content-two">
  <div class="card-contain">
    <?php if( have_rows('about_section_two_cards') ): ?>

      <?php while ( have_rows('about_section_two_cards') ) : the_row(); ?>
        <div class="card">

          <?php if(get_sub_field('text_above_headline')): ?>
            <h5><?php the_sub_field('text_above_headline'); ?></h5>
          <?php endif;?>

          <h2 style="margin-top: <?php if(get_sub_field('text_above_headline')): ?><?php echo '0'; ?><?php endif;?>"><?php the_sub_field('headline'); ?></h2>

          <?php if(get_sub_field('sub-headline')): ?>
            <p class="sub-headline"><?php the_sub_field('sub-headline'); ?></p>
          <?php endif;?>

          <?php the_sub_field('main_content'); ?>

        </div>
      <?php endwhile; ?>

    <?php endif; ?>

  </div>

  <div class="button-container">
    <?php 

      $link = get_field('about_section_two_button');

      if( $link ): ?>
        
        <a class="button-green" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>

    <?php endif; ?>
  </div>
</section>