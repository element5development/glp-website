<section class="default-banner-section" style="background-image: url('<?php the_field('banner_image'); ?>')">
  <div class="content">
    <h1><?php the_field('banner_headline'); ?></h1>
    <?php the_field('banner_content'); ?>
  </div>
</section>