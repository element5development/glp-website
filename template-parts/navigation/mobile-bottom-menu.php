<div class="mobile-bottom-menu-wrap">
  <nav class="mobile-bar">
    <ul>
      <li>
<a href="<?php echo get_permalink('105'); ?>" style="color: <?php if(get_the_id() == 105): ?> <?php echo '#C1E32E'; ?> <?php endif; ?>">
          <img src="<?= get_template_directory_uri();?>/dist/images/about-icon.svg" alt="About Us Icon">
          <h4>About</h4>
        </a>
      </li>
      <li>
        <a href="<?php echo get_permalink('107'); ?>" style="color: <?php if(get_the_id() == 107): ?> <?php echo '#C1E32E'; ?> <?php endif; ?>">
          <img src="<?= get_template_directory_uri();?>/dist/images/service-icon.svg" alt="Services Icon">
          <h4>Services</h4>
        </a>
      </li>
      <li>
        <a href="<?php echo get_permalink('109'); ?>" style="color: <?php if(get_the_id() == 109): ?> <?php echo '#C1E32E'; ?> <?php endif; ?>">
          <img src="<?= get_template_directory_uri();?>/dist/images/careers-icon.svg" alt="Careers Icon">
          <h4>Careers</h4>
        </a>
      </li>
      <li>
        <a href="<?php echo get_permalink('111'); ?>" style="color: <?php if(get_the_id() == 111): ?> <?php echo '#C1E32E'; ?> <?php endif; ?>">
          <img src="<?= get_template_directory_uri();?>/dist/images/contact-icon.svg" alt="Contact Us Icon">
          <h4>Contact</h4>
        </a>
      </li>
    </ul>
  </nav>
</div>