<header class="primary-nav-container">
  <div class="nav-wrap">
  <div class="search-icon-mobile">
    <div class="content">
      <img src="<?= get_template_directory_uri();?>/dist/images/search-icon.svg" />
      <p>Search</p>
    </div>
  </div>
    <div class="brand-wrap">
        <a class="brand" href="<?= esc_url(home_url('/')); ?>">
          <img src="<?= get_template_directory_uri();?>/dist/images/glp_logo_final.png" />
        </a>
      </div>
      
    <nav class="nav-primary">
      <div class="mobile-close-button">
        <img src="<?= get_template_directory_uri();?>/dist/images/mobile-close-button.svg" alt="Mobile Close Button">
      </div>
      <?php if (has_nav_menu('primary_nav')) :
        wp_nav_menu(['theme_location' => 'primary_nav', 'menu_class' => 'nav']);
      endif; ?>
      <div class="search-icon">
        <img src="<?= get_template_directory_uri();?>/dist/images/search-icon.svg" />
      </div>
      <div class="menu-social-contain">
        <ul>
          <?php if( have_rows('social_media', 'option') ): ?>
            <?php while ( have_rows('social_media', 'option') ) : the_row(); ?>
              <li><a href="<?php the_sub_field('social_media_url'); ?>" target="_blank"><img src="<?php the_sub_field('social_media_icon'); ?>" alt="<?php the_sub_field('social_media_name'); ?>"></a></li>
            <?php endwhile; ?>
          <?php endif; ?>
        </ul>
      </div>
    </nav>
    <div class="mobile-menu-icon">
      <div class="content">
        <img src="<?= get_template_directory_uri();?>/dist/images/mobile-menu-icon.svg" />
        <p>Menu</p>
      </div>
    </div>
  </div>
</header>
<div class="search-dropdown">
    <div class="dropdown-contain">
      <div class="search-contain">
        <form role="search" method="get" id="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
          <input type="search" placeholder="<?php echo esc_attr( 'Search...', 'presentation' ); ?>" name="s" id="search-input" value="<?php echo esc_attr( get_search_query() ); ?>" />
        </form>
      </div>
    </div>
</div>